# Makefile
prefix ?= /usr/local
version = chsip-0
date = $(shell dpkg-parsechangelog -c1 -S date | LC_ALL=C date -u +"%d %B %Y" -f -)
BIN = chsip
MAN1 = chsip.1
CFLAGS ?= -Wall

all: $(BIN)

build: chsip.c
	@$(CC) chsip.c $(CFLAGS) -o chsip

install: $(MAN1)
	@mkdir -p $(prefix)/sbin $(prefix)/share/man/man1
	@cp $(BIN) $(prefix)/sbin/
	@cp $(MAN1) $(prefix)/share/man/man1

clean:
	@rm -f *.1 *.ps *.pdf *.html $(BIN)

%.1:%.txt; txt2man -s 1 -t $* -r $(version) -d "$(date)" $< > $@
%.html:%.1; rman -f HTML $< > $@
%.ps:%.1; groff -man $< > $@
%.pdf:%.ps; ps2pdf $< > $@
